import React from 'react';
import { connect } from 'react-redux';
import { deleteNoteFromWorkspace, changeNoteColor } from '../../actions/workspaceActions';
import EditableLabel from 'react-inline-editing';
import Draggable from 'react-draggable';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw } from 'draft-js';
import { FaPencilAlt } from 'react-icons/fa';
import Dropzone from '../pictureDropzone/index'
import ColorBox from '../colorBox'
import dropdownStyle from '../workspaceNavbar/style';
import style from './style';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Constants } from './constants/noteConstants';

class Note extends React.Component {
    constructor(props) {
        super(props);
        const contentState = convertFromRaw(Constants.Content);

        this.state = {
            contentState,
            isToggleOpen: false,
            toolbarIsHidden: true,
            toolbarIsFull: false
        }
    }

    render() {
        const toolbar = this.state.toolbarIsFull ? Constants.FullToolbar : Constants.DefaultToolbar;
        const onDelete = () => this.props.deleteNoteFromWorkspace(this.props.id);
        return (
            <Draggable>
                <div className={Constants.Card} style={style.note(this.props.x + Constants.XCoordinate, this.props.y - Constants.YCoordinate)}>
                    <div className={Constants.CardHeader} style={style.note_header(this.props.note.color)}>
                        <button onClick={onDelete} type={Constants.Button} className={Constants.CloseNoteButton} aria-label={Constants.AriaLabel}>
                            <span aria-hidden={Constants.True}>&times;</span>
                        </button>
                        <div className={Constants.Dropdown} onClick={this.toggleOpen}>
                            <button type={Constants.Button} className={Constants.DropdownButton}></button>
                            {this.noteColorDropdownMenu()}
                        </div>
                        <div>
                            <FaPencilAlt size={Constants.PencilSize} />
                        </div>
                        <div style={style.label}>
                            <EditableLabel
                                text={this.props.note.title}
                                inputWidth={style.title.inputWidth}
                                inputHeight={style.title.inputHeight}
                            />
                        </div>
                    </div>
                    <div className={Constants.CardBody} style={style.note_body}>
                        <div onDoubleClick={this.showToolbar}>
                            <Editor
                                toolbarHidden={this.state.toolbarIsHidden}
                                toolbarOnFocus
                                placeholder={Constants.Placeholder}
                                color={this.props.color}
                                onContentStateChange={this.onContentStateChange}
                                toolbar={toolbar}
                                toolbarCustomButtons={[<h3 onClick={this.addMoreOptions} style={style.editor_buttons}>{this.state.toolbarIsFull ? Constants.EditorToggleAddOptionButton : Constants.EditorToggleRemoveOptionButton}</h3>]}
                            />
                        </div>
                        <Dropzone
                            id={this.props.note.id}
                        />
                    </div>
                </div >
            </Draggable>
        );
    };

    noteColorDropdownMenu = () => {
        if (this.state.isToggleOpen === true) {
            return (
                <div className={Constants.DropdownColorMenu} aria-labelledby={Constants.DropdownMenuButton} style={dropdownStyle.dropdown}>
                    <div className={Constants.FormInline} style={dropdownStyle.colors_box}>
                        {
                            this.props.colorArray.map((color) => {
                                return (
                                    ColorBox(this.props.changeNoteColor, color, this.props.id)
                                )
                            })
                        }
                    </div>
                </div>
            )
        }
    }

    toggleOpen = () => this.setState({ isToggleOpen: !this.state.isToggleOpen });

    onContentStateChange = (contentState) => {
        this.setState({
            contentState
        });
    };

    addMoreOptions = () => this.setState({
        toolbarIsFull: !this.state.toolbarIsFull
    });

    showToolbar = (event) => {
        event.stopPropagation();
        this.setState({
            toolbarIsHidden: !this.state.toolbarIsHidden
        });
    };
};

const mapStateToProps = (state, props) => ({
    note: state.workspace.noteArray.find(note => props.id === note.id),
    color: state.workspace.defaultColor,
    colorArray: state.workspace.colorArray
});

const mapDispatchToProps = (dispatch) => ({
    deleteNoteFromWorkspace: (id) => dispatch(deleteNoteFromWorkspace(id)),
    changeNoteColor: (color, id) => dispatch(changeNoteColor(color, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Note);