const shadow = '0 1px 6px 0 rgba(0, 0, 0, 0.3), 0 6px 20px 0 rgba(0, 0, 0, 0.3)'

export default {
    note: (x, y) => ({
        border: 'white solid',
        width: '20%',
        position: 'absolute',
        left: x,
        top: y,
        boxShadow: shadow
    }),
    note_header: (color) => ({
        background: color,
        height: '35%',
        width: '100%'
    }),
    note_list: {
        listStyle: 'none'
    },
    note_body: {
        paddingTop: '2%',
        cursor: 'pointer'
    },
    input: {
        height: '10%',
        width: '10%'
    },
    title: {
        inputWidth: '90%',
        inputHeight: '25px'
    },
    text: {
        inputWidth: '95%',
        inputMaxLength: '1000'
    },
    label: {
        position: "absolute",
        top: "12px",
        left: "40px"
    },
    editor_buttons: {
        paddingTop: '3%',
        color: "#ff6d00",
    }

};

