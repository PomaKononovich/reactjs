export const Constants = {
    FullToolbar: 'toolbar',
    DefaultToolbar: {
        options: ['inline', 'fontSize', 'fontFamily', 'history', 'colorPicker'],
        inline: {
            options: ['bold', 'italic', 'underline']
        }
    },
    Content: {
        entityMap: {},
        blocks: [{
            key: "637gr",
            text: "Initialized from content state.",
            type: "unstyled",
        }]
    },
    PencilSize: 12,
    Button: "button",
    Dropdown: "dropdown",
    CardHeader: "card-header",
    Card: "card",
    XCoordinate: 72,
    YCoordinate: 132,
    True: "true",
    AriaLabel: "Close",
    CloseNoteButton: "btn btn-secondary close",
    DropdownButton: "btn btn-secondary dropdown-toggle btn-sm close",
    CardBody: "card-body",
    Placeholder: "Begin typing...",
    EditorToggleAddOptionButton: "^",
    EditorToggleRemoveOptionButton: "ⱽ",
    DropdownColorMenu: "dropdown-menu show",
    DropdownMenuButton: "dropdownMenuButton",
    FormInline: "form-inline",
    
};