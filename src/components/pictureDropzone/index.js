import React from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import style from './style';
import { addPictureToTheNote } from '../../actions/workspaceActions';
import { Constants } from './constants/pictureDropzoneConstants';

class DropzonePicture extends React.Component {
    render() {
        return (
            <section style={style.dropzone}>
                <Dropzone accept={Constants.Accept} onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            <p>{Constants.DropPictureText}</p>
                        </div>
                    )}
                </Dropzone>
                <aside style={style.thumbsContainer}>
                    {this.pictureThumbs()}
                </aside>
            </section>
        );
    };

    pictureThumbs = () => {
        return (
            this.props.pictures.map(file => (
                <div style={style.thumb} key={file.name}>
                    <div style={style.thumbInner}>
                        <img alt={Constants.AltImg} src={file.preview} style={style.img} />
                    </div>
                </div>
            ))
        )
    }

    onDrop = (files) => {
        const pictures = files.map(file => Object.assign(file, {
            preview: URL.createObjectURL(file)
        }))
        this.props.addPictureToTheNote(this.props.id, pictures)
    };
};

const mapStateToProps = (state, props) => ({
    pictures: state.workspace.pictures.filter(picture => picture.noteId === props.id)
});

const mapDispatchToProps = (dispatch) => ({
    addPictureToTheNote: (id, files) => dispatch(addPictureToTheNote(id, files))
});

export default connect(mapStateToProps, mapDispatchToProps)(DropzonePicture);