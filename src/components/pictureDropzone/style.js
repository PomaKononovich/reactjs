export default {
    dropzone: {
        color: '#757575',
        border: '1px dotted #757575',
        borderRadius: '8px',
        textAlign: 'center'
    },
    img: {
        display: 'block',
        height: '100%',
        width: '100%'
    },
    thumbsContainer: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16,
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: "1%",
        border: '1px solid #eaeaea',
        marginBottom: "2%",
        marginRight: "1.5%",
        marginLeft: "1.5%",
        width: "47%",
        height: "50%",
        padding: "2%",
        boxSizing: 'border-box'
    },    
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    }
}