import React from 'react';
import Note from '../note/index';
import { connect } from 'react-redux';
import { addNoteToWorkspace } from '../../actions/workspaceActions';
import style from './style';
import WorkspaceNavbar from '../workspaceNavbar/index';

class Workspace extends React.Component {
	render() {
		return (
			<div>
				<WorkspaceNavbar
					title={this.props.title}
					color={this.props.color}
				/>
				<div onDoubleClick={(MouseEvent) => this.props.addNoteToWorkspace(MouseEvent.screenX, MouseEvent.screenY)} style={style.workspace(this.props.color)}>
					{
						this.props.noteArray.map((note) => {
							return (
								<Note
									key={note.id}
									id={note.id}
									x={note.x}
									y={note.y}
								/>
							)
						})
					}
				</div>
			</div>

		);
	};
};

const mapStateToProps = (state) => ({
	title: state.workspace.title,
	noteArray: state.workspace.noteArray,
	color: state.workspace.defaultColor
});

const mapDispatchToProps = (dispatch) => ({
	addNoteToWorkspace: (x, y) => dispatch(addNoteToWorkspace(x, y))
});

export default connect(mapStateToProps, mapDispatchToProps)(Workspace);
