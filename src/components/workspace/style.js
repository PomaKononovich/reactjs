export default {
    workspace: (color) => ({
        position: 'absolute',
        height: '100%',
        width: '100%',
        background: color
    })
};