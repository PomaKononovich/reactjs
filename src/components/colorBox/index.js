import React from 'react';
import style from './style';

const colorBox = (action, color, noteID) => <div onClick={() => action(color, noteID)} style={style.color_box(color)}></div>;

export default colorBox