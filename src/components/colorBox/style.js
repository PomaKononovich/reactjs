export default {
    color_box: (color) => ({
        background: color,
        width: '34px',
        height: '34px',
        border: '7px solid',
        borderColor: 'white',
        cursor: 'pointer',
        position: 'relative',
        outline: 'currentcolor none medium',
        float: 'left',
        borderRadius: '10px'
    })
}