export const Constants = {
    ButtonGroup: "btn-group",
    Type: "button",
    DropdownButton: "btn btn-outline-secondary dropdown-toggle",
    Toggle: "dropdown",
    ColorSettigns: "Color Settings",
    DropdownMenu: "dropdown-menu show",
    SubmitButton: "input-group mb-3",
    InputGroup: "input-group-prepend",
    AddColorButton: "btn btn-outline-secondary",
    SubmitType: "submit",
    Submit: "Submit",
    AddButtonText: "Add",
    ImputType: "text",
    FormControl: "form-control",
    Placeholder: "#color"
}