export default {
    navbar: (color) => ({
        background: color
    }),
    dropdown: {
        minWidth: '40px',
        maxWidth: '243px',
        padding: '0px'
    },
    img: {
        display: 'block',
    },
    thumbsContainer: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 16,
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        marginBottom: 8,
        marginRight: 8,
        width: '45%',
        height: '45%',
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    },
    gallery: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 50,
        x: 0,
        y: 0,
        zIndex: 1,
        overflow: '',
        width: "1000%",
        height: "1000%",
        right: "700%",
        flex: 1,
        background: "#d50000"
    },
    submitButton: {
        maxHeight: "21px"
    }
};