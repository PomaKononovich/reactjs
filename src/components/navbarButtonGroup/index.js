import React from 'react';
import style from './style'
import { connect } from 'react-redux';
import { changeNoteColor, changeNavbarColor } from '../../actions/workspaceActions';
import ColorBox from '../colorBox/index'
import Gallery from '../gallery/index';
import { Constants } from './constants/navbarButtonGropConstants';

class NavbarButtonGroup extends React.Component {
    state = {
        value: ''
    };

    render() {
        return (
            <div className={Constants.ButtonGroup}>
                <Gallery />
                <div className={Constants.ButtonGroup}>
                    <button onClick={this.toggleOpen} type={Constants.Type} className={Constants.DropdownButton} data-toggle={Constants.Toggle}>
                        {Constants.ColorSettigns}
                    </button>
                    {this.openToggleMenu()}
                </div>
            </div>
        );
    };

    openToggleMenu = () => {
        if (this.state.isToggleOpen === true) {
            return (
                <div className={Constants.DropdownMenu} style={style.dropdown}>
                    {
                        this.props.colorArray.map((color) => {
                            return (
                                ColorBox(this.props.changeNavbarColor, color)
                            )
                        })
                    }
                    <form class={Constants.SubmitButton} onSubmit={this.handleSubmit} style={style.submitButton}>
                        <div class={Constants.InputGroup}>
                            <button class={Constants.AddColorButton} type={Constants.SubmitType} value={Constants.Submit}>{Constants.AddButtonText}</button>
                        </div>
                        <input type={Constants.ImputType} class={Constants.FormControl} placeholder={Constants.Placeholder} value={this.state.value} onChange={this.handleChange} />
                    </form>
                </div>
            )
        }
    }

    handleChange = (event) => this.setState({ value: event.target.value });

    handleSubmit = (event) => {
        this.setState({ isToggleOpen: !this.state.isToggleOpen });
        this.props.colorArray.push(this.state.value)
        event.preventDefault();
    };

    toggleOpen = () => {
        this.setState({ isToggleOpen: !this.state.isToggleOpen });
    }
};

const mapStateToProps = (state) => ({
    color: state.workspace.defaultColor,
    colorArray: state.workspace.colorArray,
});

const mapDispatchToProps = (dispatch) => ({
    changeNavbarColor: (color) => dispatch(changeNavbarColor(color)),
    changeNoteColor: (color, id) => dispatch(changeNoteColor(color, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(NavbarButtonGroup);