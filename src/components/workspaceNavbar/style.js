export default {
    navbar: (color) => ({
        background: color
    }),
    dropdown: {
        minWidth: '40px',
        maxWidth: '243px',
        padding: '0px'
    },
    dropzone: {
        color: '#757575',
        border: '1px dotted #757575',
        textAlign: 'center'
    },
    img: {
        display: 'block',
        height: '200%',
        width: '200%'
    },
    thumbsContainer: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16,
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    },
    gallery: {
        position: 'absolute',
        x: 0,
        y: 0,
        flex: 1,
        background: "#d50000"
    },
    label: {
        position: "absolute",
        left: '35px'
    },
    label_pencil: {
        paddingBottom: '0.5%'
    }
};