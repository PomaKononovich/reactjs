import React from 'react';
import style from './style'
import EditableLabel from 'react-inline-editing';
import NavbarButtonGroup from '../navbarButtonGroup/index';
import { FaPencilAlt } from 'react-icons/fa';
import { Constants } from './constants/workspaceNavbarConstants';

class WorkspaceNavbar extends React.Component {
    render() {
        return (
            <div>
                <nav className={Constants.ClassNames.Navbar} style={style.navbar(this.props.color)}>
                    <div style={style.label_pencil}>
                        <FaPencilAlt size={Constants.PencilSize} />
                    </div>
                    <div style={style.label}>
                        <EditableLabel
                            text={this.props.title}
                        />
                    </div>
                    <div className={Constants.ClassNames.ButtonGroup}>
                        <ul className={Constants.ClassNames.ButtonGroupList}>
                        </ul>
                        <NavbarButtonGroup />
                    </div>
                </nav>
            </div>
        );
    };
};

export default WorkspaceNavbar;