export const Constants = {
    PencilSize: 12,
    ClassNames: {
        Navbar: "navbar navbar-expand-lg",
        ButtonGroup: "collapse navbar-collapse",
        ButtonGroupList: "mr-auto"
    },
};