import React from 'react';
import style from './style';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { showGallery, closeGallery, showPictureView, closePictureView, showNextPicture, showPreviousPicture } from '../../actions/galleryActions';
import { FaArrowCircleLeft } from 'react-icons/fa';
import { FaArrowCircleRight } from 'react-icons/fa';
import { Constants } from './constats/galleryConstants';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        height: '85%',
        width: '72%',
        background: ''
    }
};

class Gallery extends React.Component {
    render() {
        return (
            <div className={Constants.ButtonGroup}>
                <button onClick={this.props.showGallery} type={Constants.Type} className={Constants.Button}>
                    {Constants.Gallery}
                </button>
                <Modal
                    isOpen={this.props.isGalleryOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    ariaHideApp={false}
                >
                    <h2 ref={subtitle => this.subtitle = subtitle}>{Constants.Gallery}</h2>
                    {this.props.isPictureToShowExists ? this.galleryBody() : this.pictureView()}
                    <button onClick={this.props.closeGallery} type={Constants.Type} className={Constants.CloseButton} aria-label={Constants.AriaLable} style={style.modalCloseButton}>
                        <span aria-hidden={Constants.ArianHidden}>&times;</span>
                    </button>
                </Modal>
            </div>
        )
    };

    pictureView = () => {
        return (
            <div>
                <div style={style.leftArrow}>
                    <FaArrowCircleLeft onClick={() => this.props.showPreviousPicture(this.props.pictures)} size={Constants.ArrowSize} />                </div>
                <img alt={Constants.AltImg} onClick={this.props.closePictureView}
                    src={this.props.pictureToShow.preview}
                    style={style.soloImg}
                />
                <div style={style.rightArrow}>
                    <FaArrowCircleRight onClick={() => this.props.showNextPicture(this.props.pictures)} size={Constants.ArrowSize} />
                </div >
            </div >
        )
    }

    galleryBody = () => {
        return (
            <div>
                {this.props.pictures.map(file => (
                    <div style={style.thumb}>
                        <div style={style.thumbInner}>
                            <img alt={Constants.AltImg} onClick={() => this.props.showPictureView(file)} key={`${file.size} + ${file.name}`}
                                src={file.preview}
                                style={style.img}
                            />
                        </div>
                    </div>
                ))}
            </div>
        )
    }
};

const mapStateToProps = (state) => ({
    pictures: state.workspace.pictures,
    isGalleryOpen: state.gallery.isGalleryOpen,
    isPictureToShowExists: state.gallery.isPictureToShowExists,
    pictureToShow: state.gallery.pictureToShow,
});

const mapDispatchToProps = (dispatch) => ({
    showGallery: () => dispatch(showGallery()),
    closeGallery: () => dispatch(closeGallery()),
    showPictureView: (file) => dispatch(showPictureView(file)),
    closePictureView: () => dispatch(closePictureView()),
    showNextPicture: (file) => dispatch(showNextPicture(file)),
    showPreviousPicture: (file) => dispatch(showPreviousPicture(file))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);