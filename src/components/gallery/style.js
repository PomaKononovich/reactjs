export default {
    img: {
        display: 'block',
        width: '100%',
        height: '100%'
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        marginBottom: 8,
        marginRight: 8,
        width: '32.6%',
        height: '30%',
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden',
        width: '100%',
        height: '100%',
    },
    leftArrow: {
        position: "absolute",
        top: '6%',
        left: '47.5%',
        right: 'auto',
        bottom: 'auto',
        transform: 'translate(-50%, -50%)',
    },
    rightArrow: {
        position: "absolute",
        top: '6%',
        left: '52.5%',
        right: 'auto',
        bottom: 'auto',
        transform: 'translate(-50%, -50%)',
    },
    soloImg: {
        width: '100%',
        height: '570px'
    },
    modalCloseButton: {
        position: "absolute",
        left: '97%',
        top: '4%'
    }  
};