export const Constants = {
    Gallery: "Gallery",
    ButtonGroup: "btn-group",
    Type: "button",
    Button: "btn btn-outline-secondary",
    CloseButton: "btn btn-secondary close",
    AriaLable: "Close",
    ArianHidden: "true",
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        height: '85%',
        width: '72%',
        background: ''
    },
    ArrowSize: 30,
    AltImg: "Picture"
}