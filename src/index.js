import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { workspaceReducer } from './reducers/workspaceReducers'
import { galleryReducer } from './reducers/galleryReducer'
import thunk from 'redux-thunk';

import App from './components/App';

const reducers = combineReducers({
	workspace : workspaceReducer,
	gallery: galleryReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

render (
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)