import { ACTION_TYPES } from '../constants/actionTypes'

export const changeWorkspaceTitle = (title) => dispatch => {
    dispatch(changeWorkspaceTitleSuccess(title))
};

export const addNoteToWorkspace = (x, y) => dispatch => {
    dispatch(addNoteToWorkspaceSuccess(x, y))
};

export const deleteNoteFromWorkspace = (id) => dispatch => {
    dispatch(deleteNoteFromWorkspaceSuccess(id))
};

export const changeNavbarColor = (id) => dispatch => {
    dispatch(changeNavbarColorSuccess(id))
};

export const changeNoteColor = (color, id) => dispatch => {
    dispatch(changeNoteColorSuccess(color, id))
};

export const addPictureToTheNote = (id, files) => dispatch => {
    dispatch(addPictureToTheNoteSuccess(id, files))
};

export const showNextPicture = (file) => dispatch => {
    dispatch(showNextPictureSuccess(file))
};

export const showPreviousPicture = (file) => dispatch => {
    dispatch(showPreviousPictureSuccess(file))
};

const changeWorkspaceTitleSuccess = (title) => (
    {
        type: ACTION_TYPES.CHANGE_WORKSPACE_TITLE,
        title: title
    }
);

const addNoteToWorkspaceSuccess = (x, y) => (
    {
        type: ACTION_TYPES.ADD_NOTE,
        x: x,
        y: y
    }
);

const deleteNoteFromWorkspaceSuccess = (id) => (
    {
        type: ACTION_TYPES.DELETE_NOTE,
        id: id
    }
);

const changeNavbarColorSuccess = (id) => (
    {
        type: ACTION_TYPES.CHANGE_COLOR,
        id: id
    }
);

const changeNoteColorSuccess = (color, id) => (
    {
        type: ACTION_TYPES.CHANGE_NOTE_COLOR,
        color: color,
        id: id
    }
);


const addPictureToTheNoteSuccess = (id, files) => (
    {
        type: ACTION_TYPES.ADD_PICTURE_TO_THE_NOTE,
        id: id,
        files: files
    }
);

const showNextPictureSuccess = (file) => (
    {
        type: ACTION_TYPES.SHOW_NEXT_PICTURE,
        file: file
    }
);

const showPreviousPictureSuccess = (file) => (
    {
        type: ACTION_TYPES.SHOWE_PREVIOUS_PICTURE,
        file: file
    }
);