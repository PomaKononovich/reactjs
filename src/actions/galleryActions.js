import { ACTION_TYPES } from '../constants/actionTypes'

export const showGallery = () => dispatch => {
    dispatch(showGallerySuccess())
};

export const closeGallery = () => dispatch => {
    dispatch(closeGallerySuccess())
};

export const showPictureView = (file) => dispatch => {
    dispatch(showPictureViewSuccess(file))
};

export const closePictureView = () => dispatch => {
    dispatch(closePictureViewSuccess())
};

export const showNextPicture = (file) => dispatch => {
    dispatch(showNextPictureSuccess(file))
};

export const showPreviousPicture = (file) => dispatch => {
    dispatch(showPreviousPictureSuccess(file))
};

const showGallerySuccess = () => (
    {
        type: ACTION_TYPES.SHOW_GALLERY
    }
);

const closeGallerySuccess = () => (
    {
        type: ACTION_TYPES.CLOSE_GALLERY
    }
);

const showPictureViewSuccess = (file) => (
    {
        type: ACTION_TYPES.SHOW_PICTURE_VIEW,
        file: file
    }
);

const closePictureViewSuccess = () => (
    {
        type: ACTION_TYPES.CLOSE_PICTURE_VIEW,
    }
);

const showNextPictureSuccess = (file) => (
    {
        type: ACTION_TYPES.SHOW_NEXT_PICTURE,
        file: file
    }
);

const showPreviousPictureSuccess = (file) => (
    {
        type: ACTION_TYPES.SHOWE_PREVIOUS_PICTURE,
        file: file
    }
);