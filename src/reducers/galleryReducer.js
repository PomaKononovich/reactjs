import { ACTION_TYPES } from '../constants/actionTypes'

const initState = {
    isGalleryOpen: false,
    isPictureToShowExists: true,
    pictures: [],
    pictureToShow: [],
    endOfPictureArray: false
};

export const galleryReducer = (state = initState, action) => {
    switch (action.type) {
        case ACTION_TYPES.SHOW_GALLERY: {
            const galleryIsOpen = !state.isGalleryOpen
            return { ...state, isGalleryOpen: galleryIsOpen };
        }
        case ACTION_TYPES.CLOSE_GALLERY: {
            const galleryIsClosed = !state.isGalleryOpen
            return { ...state, isGalleryOpen: galleryIsClosed };
        }
        case ACTION_TYPES.SHOW_PICTURE_VIEW: {
            const isPictureToShowExists = !state.isPictureToShowExists
            state.pictureToShow = action.file
            return { ...state, isPictureToShowExists: isPictureToShowExists };
        }
        case ACTION_TYPES.CLOSE_PICTURE_VIEW: {
            const isPictureToShowExists = !state.isPictureToShowExists
            return { ...state, isPictureToShowExists: isPictureToShowExists };
        }
        case ACTION_TYPES.SHOW_NEXT_PICTURE: {
            let a = undefined;
            for (var i = 0; i < action.file.length; i++) {
                if (action.file[i] === state.pictureToShow) {
                    if (i === action.file.length - 1) {
                        a = state.pictureToShow
                    }
                    else {
                        a = (action.file[i + 1])
                    }
                }
            }
            state.pictureToShow = a
            return { ...state };
        }
        case ACTION_TYPES.SHOWE_PREVIOUS_PICTURE: {
            let b = undefined;
            for (var q = 0; q < action.file.length; q++) {
                if (action.file[q] === state.pictureToShow) {
                    if (q === 0) {
                        b = state.pictureToShow
                    }
                    else {
                        b = (action.file[q - 1])
                    }
                }
            }
            state.pictureToShow = b
            return { ...state };
        }
        default: return state;
    }
};