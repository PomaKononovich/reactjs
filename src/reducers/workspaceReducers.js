import { ACTION_TYPES } from '../constants/actionTypes'

const initState = {
	title: 'Workspace',
	noteArray: [],
	colorArray: ['#CC0000', '#FF8800', '#ffff00', '#007E33', '#0d47a1', '#9933CC', '#f1f1f1', '#42a5f5', '#18ffff', '#ff9800', '#757575', '#2BBBAD'],
	defaultColor: null,
	defaultNoteColor: null,
	pictures: [],
};

export const workspaceReducer = (state = initState, action) => {
	switch (action.type) {
		case ACTION_TYPES.CHANGE_WORKSPACE_TITLE: {
			return { ...state, title: action.title };
		}
		case ACTION_TYPES.ADD_NOTE: {
			const newnote = { id: (new Date()).getTime(), title: "New note", text: "", color: "", picture: "", x: action.x, y: action.y }
			const newNoteArray = [...state.noteArray, newnote];
			return { ...state, noteArray: newNoteArray };
		}
		case ACTION_TYPES.DELETE_NOTE: {
			const newNoteArray = state.noteArray.filter(note => action.id !== note.id);
			return { ...state, noteArray: newNoteArray };
		}
		case ACTION_TYPES.CHANGE_COLOR: {
			state.defaultColor = action.id
			return { ...state };
		}
		case ACTION_TYPES.CHANGE_NOTE_COLOR: {
			const note = state.noteArray.find(note => action.id === note.id)
			note.color = action.color
			return { ...state };
		}
		case ACTION_TYPES.ADD_PICTURE_TO_THE_NOTE: {
			const newPictureArray = action.files.map(file => {
				file.noteId = action.id;
				return file;
			})
			return { ...state, pictures: [...state.pictures, ...newPictureArray] };
		}
		default: return state;
	}
};